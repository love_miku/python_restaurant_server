# Python Restaurant Server

## Setup

```
$ python -m venv venv
$ venv\Script\activate
$ pip install flask flask-mysql
```

## Run

```
$ python app.py
```

Brower open http://localhost:5000/
