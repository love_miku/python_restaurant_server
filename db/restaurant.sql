-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2021-03-12 12:42:28
-- 伺服器版本： 10.4.17-MariaDB
-- PHP 版本： 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `restaurant`
--

-- --------------------------------------------------------

--
-- 資料表結構 `appitizer`
--

CREATE TABLE `appitizer` (
  `id` int(11) NOT NULL,
  `dish_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `picture_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `appitizer`
--

INSERT INTO `appitizer` (`id`, `dish_name`, `price`, `amount`, `picture_url`) VALUES
(1, 'salad', 199, 21, 'salad.jpg'),
(2, 'takoyaki', 120, 17, 'takoyaki.jpg'),
(3, 'delish-cranberry-brie', 150, 10, 'delish-cranberry-brie.jpg'),
(7, 'marshmellow', 50, 10, 'marshmellow.jpg');

-- --------------------------------------------------------

--
-- 資料表結構 `main_course`
--

CREATE TABLE `main_course` (
  `id` int(11) NOT NULL,
  `dish_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `picture_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `main_course`
--

INSERT INTO `main_course` (`id`, `dish_name`, `price`, `amount`, `picture_url`) VALUES
(1, 'pizza', 250, 15, 'pizza.jpg'),
(2, 'steak', 300, 5, 'steak.jpg'),
(3, 'hamburger', 150, 50, 'hamburger.jpg');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `appitizer`
--
ALTER TABLE `appitizer`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `main_course`
--
ALTER TABLE `main_course`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `appitizer`
--
ALTER TABLE `appitizer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `main_course`
--
ALTER TABLE `main_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
