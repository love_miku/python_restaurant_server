import os


from flask import Flask, request, render_template, send_from_directory

from flaskext.mysql import MySQL

import json

#__author__ = 'ibininja'

app = Flask(__name__)
mysql = MySQL()
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_PORT'] = 3306
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'restaurant'
app.config['MYSQL_DATABASE_CHARSET'] = 'utf8mb4'
mysql.init_app(app)


APP_ROOT = os.path.dirname(os.path.abspath(__file__))


@app.route("/mysql")
def get_mysql():
    words = ["appitizer","main_course"]
    list_of = []
    for i in words:
        conn = mysql.connect()
        cursor = conn.cursor()
        query = "SELECT * FROM `"+i+"` WHERE 1"
        cursor.execute(query)
        result = cursor.fetchall()
        result = list(result)
        for k in range(len(result)):
            l_temp = []
            for j in range(len(result[k])):
                l_temp.append(str(result[k][j]))
            l_temp.append(str(result[k][1])+"_price")
            l_temp.append(str(result[k][1])+"_amount")
            result[k] = l_temp
            print(l_temp)
            #k = list(k)
            #k.append(str(k[1])+'_price')
            #k.append(str(k[1])+'_amount')
        list_of.append([i,result])

    list_of=list(list_of)
    #print(list_of)
    #print(result)
    #for row in result:
        #print(row)
    print(list_of)
    return render_template("mysql.html",list_of_names = list_of)


#test
@app.route("/orders",methods=["POST","GET"])
def orders():
    words = ["appitizer","main_course"]
    list_of = []
    for i in words:
        conn = mysql.connect()
        cursor = conn.cursor()
        query = "SELECT * FROM `"+i+"` WHERE 1"
        cursor.execute(query)
        result = cursor.fetchall()
        result = list(result)
        for k in range(len(result)):
            l_temp = []
            for j in range(len(result[k])):
                l_temp.append(str(result[k][j]))
            l_temp.append(str(result[k][1])+"_price")
            l_temp.append(str(result[k][1])+"_amount")
            l_temp[4] = "http://192.168.43.26:5000/upload/" + l_temp[4]
            result[k] = l_temp
            #print(l_temp)
            #k = list(k)
            #k.append(str(k[1])+'_price')
            #k.append(str(k[1])+'_amount')
        list_of.append([i,result])

    list_of=list(list_of)

    return json.dumps(list_of)





L = []



@app.route("/orders_return",methods=["POST"])
def orders_return():
    #data = request.form("data")
    in_l = [0,0]
    addcnt = [] 
    print(request.form.items())
    for key, value in request.form.items():
        in_l[0] = key
        in_l[1] = value
        
        print(key)
        print(value)

        #print(in_l[0]+"  "+in_l[1])
        L_split = in_l[0].split('!')
        if(in_l[0]=="table"):
            addcnt.append(in_l[1])
            continue
        elif(in_l[1]=="table"):
            continue
        tb_name = L_split[0]
        dish_name = L_split[1]

    

        conn = mysql.connect()
        cursor = conn.cursor()
        query = "SELECT * FROM `"+str(tb_name)+"` WHERE `dish_name` = '"+str(dish_name)+"'"
        cursor.execute(query)
        result = cursor.fetchall()  
        result = list(result)
        amt = int(result[0][3])
        amt = amt - int(in_l[1])
        
        
        cnt = [tb_name,dish_name,in_l[1]]
        addcnt.append(cnt)
    
        conn = mysql.connect()
        cursor = conn.cursor()
        query = "UPDATE "+str(tb_name)+" SET amount= "+str(amt)+" WHERE dish_name='"+dish_name+"'"
        cursor.execute(query)
        conn.commit()
        

    L.append(addcnt)
    print(L)
    return "200"

@app.route("/orders_view")
def orders_view():
    return render_template("orders_view.html")

@app.route("/orders_content",methods=["POST"])
def orders_content():
    return {'context':L }

@app.route("/orders_erase",methods=["POST"])
def orders_erase():
    erase_tg = request.form.get("erase")
    del L[int(erase_tg)]
    return render_template("orders_view.html")
    print("")

















#test


@app.route("/change",methods=["POST"])
def change():
    img_url = request.form.get("img_url")
    img_nm = request.form.get("img_nm")
    classify = request.form.get("classify")
    price = int(request.form.get("price"))
    remain = int(request.form.get("remain"))

    
    conn = mysql.connect()
    cursor = conn.cursor()
    query = "UPDATE "+str(classify)+" SET price="+str(price)+" WHERE dish_name='"+img_nm+"'"
    cursor.execute(query)
    #conn.commit()
    query = "UPDATE "+str(classify)+" SET amount="+str(remain)+" WHERE dish_name='"+img_nm+"'"
    cursor.execute(query)
    conn.commit()

    return render_template("full_done.html")

@app.route("/get_st",methods=["POST"])
def get_st():

    words = ["appitizer","main_course"]
    list_of= []
    
    for i in words:
        conn = mysql.connect()
        cursor = conn.cursor()
        query = "SELECT dish_name , price , amount FROM `"+i+"` WHERE 1"
        cursor.execute(query)
        result = cursor.fetchall()
        result = list(result)
        for k in range(len(result)):
            l_temp = []
            for j in range(len(result[k])):
                l_temp.append(str(result[k][j]))
            l_temp.append(str(result[k][0])+"_price")
            l_temp.append(str(result[k][0])+"_amount")
            result[k] = l_temp
    
        list_of.append(result)

    return {words[0]:list_of[0] , words[1]:list_of[1]}

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/up")
def index():
    return render_template("upload.html")

@app.route("/upload", methods=["POST"])
def upload():
    target = os.path.join(APP_ROOT, 'images/')
    print(target)
    if not os.path.isdir(target):
            os.mkdir(target)
    else:
        print("Couldn't create upload directory: {}".format(target))
    print(request.files.getlist("file"))
    img_we = []
    for upload in request.files.getlist("file"):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "/".join([target, filename])
        print ("Accept incoming file:", filename)
        
        print ("Save it to:", destination)
        upload.save(destination)
        img_we.append(os.path.splitext(upload.filename)[0])
    size = len(request.files.getlist("file"))
    # return send_from_directory("images", filename, as_attachment=True)
    return render_template("complete.html", image_name_list = request.files.getlist("file") , image_name_we = img_we , size = size)

@app.route("/com_up",methods=["POST"])
def com_up():
    img_url = request.form.get("img_url")
    img_nm = request.form.get("img_nm")
    classify = request.form.get("classify")
    price = int(request.form.get("price"))
    remain = int(request.form.get("remain"))
    

    conn = mysql.connect()
    cursor = conn.cursor()
    query = "INSERT INTO "+classify+" ( dish_name , price , amount , picture_url ) VALUES ('%s',%d,%d,'%s')" % (img_nm,price,remain,img_url)
    cursor.execute(query)
    conn.commit()
    return render_template("full_done.html")






@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("images", filename)

@app.route('/gallery')
def get_gallery():
    image_names = os.listdir('./images')
    print(image_names)
    return render_template("gallery.html", image_names=image_names)

@app.route('/test')
def test():
    return render_template("test.html")

@app.route('/test3',methods=["POST"])
def test3():


    img_url = request.form.get("ad_id")
    print(img_url)
    return render_template("gallery.html")





if __name__ == "__main__":
   app.run(host='0.0.0.0',port=5000, debug=True)

